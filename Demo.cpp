#include <iostream>
using namespace std;
#include "Demo.h"


int main()
{
	Investment inv;
	int counter=0;
	
	cout << endl << endl << "	How To Become A Millionaire" << endl << endl;
	cout << endl << "Initial investment of $" << inv.getinitial() << endl;
	cout << endl << "Monthly investments of $" << inv.getmonthly() << endl;
	cout << endl << "Anual Interest rate of 8.3%" << endl;
	
	
	cout << endl << endl << "	      End of year 1" <<endl;
	inv.setval();
	cout << endl << "Investments: $" << inv.getval() << endl;
	counter++;
	
	
	while (inv.getval() < 1000000)
	{
		inv.setval();
		counter++;
	}
	
	cout << endl << endl << "		  Result" << endl << endl;
	cout << "It would take a total of " << counter << " years in order to become a millionaire" << endl << endl;
	
	
return 0;	
}
